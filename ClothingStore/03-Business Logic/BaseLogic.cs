﻿using System;

namespace Seldat {
    public class BaseLogic : IDisposable {
        protected ClothingStoreEntities DB = new ClothingStoreEntities();

        public void Dispose() {
            DB.Dispose();
        }
    }
}
